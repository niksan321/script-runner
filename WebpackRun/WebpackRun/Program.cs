﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace WebpackRun
{
    class Program
    {
        private static string[] _ignoredDirNames = { "packages", ".git", "node_modules" };
        private static CmdArgs _command;

        static void Main(string[] args)
        {

            Console.WriteLine("Start");
            _command = Args.Configuration.Configure<CmdArgs>().CreateAndBind(args);

            var path = _command.Path ?? AppDomain.CurrentDomain.BaseDirectory;

            Console.WriteLine($"Path: {path}");

            var dr = new DirectoryInfo(path);
            Search(dr);
            Console.WriteLine("End, press any key");
            Console.ReadKey();
        }

        static void Search(DirectoryInfo dr)
        {
            if (_ignoredDirNames.Contains(dr.Name))
            {
                return;
            }

            if (WebpackExists(dr))
            {
                WebpackRun(dr.FullName);
            }

            var dirs = dr.GetDirectories();
            foreach (var directoryInfo in dirs)
            {
                Search(directoryInfo);
            }
        }


        static void WebpackRun(string path)
        {
            var cmd = _command.Command;
            Console.WriteLine($"Path run: {path}");
            Console.WriteLine($"Command run: {cmd}");
            ExecuteCommand($@"/c {cmd}", path);
        }

        static bool WebpackExists(DirectoryInfo dr)
        {
            var fi = dr.GetFiles();
            foreach (var info in fi)
            {
                if (info.Name == _command.SearchFileName)
                {
                    return true;
                }
            }
            return false;
        }

        static void ExecuteCommand(string command, string dir)
        {
            var p = new Process();
            var startInfo = new ProcessStartInfo
            {
                FileName = "cmd.exe",
                Arguments = command,
                WorkingDirectory = dir
            };
            p.StartInfo = startInfo;
            p.Start();
            p.WaitForExit();
        }

    }
}
