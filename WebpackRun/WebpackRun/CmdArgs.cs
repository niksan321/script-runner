﻿namespace WebpackRun
{
    public class CmdArgs
    {
        public string Path { get; set; }
        public string SearchFileName { get; set; }
        public string Command { get; set; }
    }
}
